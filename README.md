**Описание:**

- Android приложение, задумывалось для домашнего бюджетирования. Ведется учет расходов и реализована отчетность.
- Демонстрация material design.
- Реализовано на основе MVP pattern.

**Технологический стек:**

- Kotlin
- ORM Room
- MVP pattern
- Dagger 2
- recycleview
- butterknife
- MPAndroidChart - для графиков (отчетности)
