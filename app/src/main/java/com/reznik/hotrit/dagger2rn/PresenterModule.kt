package com.reznik.hotrit.dagger2rn

import com.reznik.hotrit.models.ArticleModel
import com.reznik.hotrit.models.ExpensesModel
import com.reznik.hotrit.presenters.ArticlePresenter
import com.reznik.hotrit.presenters.ExpensesPresenter
import com.reznik.hotrit.models.AppDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class PresenterModule {

    @Provides
    @Singleton
    fun provideExpensesPresenter(expensesModel: ExpensesModel): ExpensesPresenter {
        return ExpensesPresenter(expensesModel)
    }

    @Provides
    @Singleton
    fun provideExpensesModel(appDatabase: AppDatabase): ExpensesModel {
        return ExpensesModel(appDatabase.expensesDao())
    }

    @Provides
    @Singleton
    fun provideArticlePresenter(articleModel: ArticleModel): ArticlePresenter {
        return ArticlePresenter(articleModel)
    }

    @Provides
    @Singleton
    fun provideArticleModel(appDatabase: AppDatabase): ArticleModel {
        return ArticleModel(appDatabase.articleDao())
    }
}