package com.reznik.hotrit.dagger2rn

import android.content.Context
import com.reznik.hotrit.domen.ArticleAdapter
import com.reznik.hotrit.domen.ExpensesAdapter
import com.reznik.hotrit.models.AppDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AdapterModule {

    @Provides
    @Singleton
    fun provideExpensesAdapter(context: Context, appDatabase: AppDatabase): ExpensesAdapter {
        return ExpensesAdapter(context, appDatabase.articleDao())
    }

    @Provides
    @Singleton
    fun provideArticleAdapter(context: Context): ArticleAdapter {
        return ArticleAdapter(context)
    }
}