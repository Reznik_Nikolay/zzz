package com.reznik.hotrit.models

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.reznik.hotrit.domen.Article
import com.reznik.hotrit.domen.Expenses
import com.reznik.hotrit.models.dao.ArticleDao
import com.reznik.hotrit.models.dao.ExpensesDao
import com.reznik.hotrit.utils.Converters

@Database(entities = [Article::class, Expenses::class], version = 1, exportSchema = false)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun articleDao(): ArticleDao
    abstract fun expensesDao(): ExpensesDao
}