package com.reznik.hotrit.contracts

import com.reznik.hotrit.domen.Expenses

interface ExpensesContractView {
    fun showExpenses(expenses: List<Expenses>)
    fun showToast(resId: Int)
}