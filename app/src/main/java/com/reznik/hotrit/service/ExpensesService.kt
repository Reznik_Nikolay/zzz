package com.reznik.hotrit.service

import com.reznik.hotrit.models.dao.ExpensesDao
import com.reznik.hotrit.utils.DateUtilsRN
import java.util.*

class ExpensesService(
    private val expensesDao: ExpensesDao
) {

    fun calculateAmountExpensesForDate(calendar: Calendar) : Double {
        val timeInMillis = calendar.timeInMillis
        return expensesDao.getAmountForPeriod(Date(DateUtilsRN.getStartOfDayInMillis(timeInMillis)), Date(DateUtilsRN.getEndOfDayInMillis(timeInMillis)))
    }
}