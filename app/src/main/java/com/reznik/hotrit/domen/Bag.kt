package com.reznik.hotrit.domen

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable
import java.util.*

@Entity(tableName = "bag")
class Bag (

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Long = 0,

    @ColumnInfo(name = "name")
    var name: String? = null,

    @ColumnInfo(name = "uuid_rel")
    var uuidRel: UUID? = null

) : Serializable {
    override fun toString() = "$name"
}