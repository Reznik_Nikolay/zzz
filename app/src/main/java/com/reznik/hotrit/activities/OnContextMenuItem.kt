package com.reznik.hotrit.activities

interface OnContextMenuItem {
    fun onClickItem(valueObj: Any, command: Int)
}