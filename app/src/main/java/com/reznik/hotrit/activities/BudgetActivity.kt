package com.reznik.hotrit.activities

import android.app.Activity
import android.os.Bundle
import android.view.View
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.reznik.hotrit.R

class BudgetActivity : Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_budget)
        init()
    }

    //Ничего не делаем, т.к. используем BottomNavigationView
    override fun onBackPressed() {}

    fun init() {
        val bottomNavigationView = findViewById<View>(R.id.bottom_navigation) as BottomNavigationView
        bottomNavigationView.linksActivity(R.id.budgetActivityMA, this)
    }
}