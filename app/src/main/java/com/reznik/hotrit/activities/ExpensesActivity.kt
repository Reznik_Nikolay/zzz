package com.reznik.hotrit.activities

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.ButterKnife
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.reznik.hotrit.App
import com.reznik.hotrit.R
import com.reznik.hotrit.contracts.ExpensesContractView
import com.reznik.hotrit.domen.Expenses
import com.reznik.hotrit.domen.ExpensesAdapter
import com.reznik.hotrit.presenters.ExpensesPresenter
import com.reznik.hotrit.utils.*
import javax.inject.Inject

class ExpensesActivity : AppCompatActivity(), ExpensesContractView {

    @Inject
    lateinit var presenter: ExpensesPresenter

    @Inject
    lateinit var expensesAdapter: ExpensesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_expenses)
        ButterKnife.bind(this)
        App.appComponent.injectExpensesActivity(this)

        init()
    }

    override fun onStart() {
        super.onStart()
        presenter.viewIsReady()
    }

    //Ничего не делаем, т.к. используем BottomNavigationView
    override fun onBackPressed() {}

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }

    //общее меню, инициализация
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_expenses, menu)
        return super.onCreateOptionsMenu(menu)
    }

    //общее меню, реализация
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.item_menu_expenses_action_clear -> deleteAllExpenses()
        }
        return super.onOptionsItemSelected(item)
    }

    fun deleteAllExpenses() {
        presenter.clear()
    }

    override fun showExpenses(expenses: List<Expenses>) {
        expensesAdapter.setData(expenses)
    }

    override fun showToast(resId: Int) {
        Toast.makeText(this, resId, Toast.LENGTH_SHORT).show()
    }

    fun addRecordExpense(view: View) {
        startActivity(Intent(this, ExpensesEnter::class.java))
    }

    private fun init() {
        val bottomNavigationView = findViewById<View>(R.id.bottom_navigation) as BottomNavigationView
        bottomNavigationView.linksActivity(R.id.expensesActivityMA, this)

        presenter.view = this
        presenter.attachView(this)

        expensesAdapter = expensesAdapter.apply {
            onContextMenuItem = object : OnContextMenuItem {
                override fun onClickItem(valueObj: Any, command: Int) {
                    when (command) {
                        CONTEXT_MENU_ITEM_EDIT -> {
                            startActivity(Intent(context, ExpensesEnter::class.java).apply {
                                putExtra(EDIT_EXPENSES, true)
                                putExtra(Expenses::class.simpleName, valueObj as Expenses)
                            })
                        }
                        CONTEXT_MENU_ITEM_DELETE -> {
                            presenter.delete(valueObj)
                            Toast.makeText(context, "Удалён $valueObj", Toast.LENGTH_SHORT).show()
                        }
                    }
                }
            }
        }

        findViewById<RecyclerView>(R.id.listExpenses).apply {
            layoutManager = LinearLayoutManager(context).apply { orientation = LinearLayoutManager.VERTICAL }
            adapter = expensesAdapter
        }
    }
}
