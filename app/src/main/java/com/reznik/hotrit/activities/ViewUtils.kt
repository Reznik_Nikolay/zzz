package com.reznik.hotrit.activities

import android.app.Activity
import android.content.Intent
import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.reznik.hotrit.R

/**
 * Функция расширения - для ограничения ввода числовых значений
 */
fun EditText.limitedNumberInput() {
    addTextChangedListener(object : TextWatcher {
        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
        override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
        override fun afterTextChanged(editable: Editable) {
            //Считываем вводимый текст
            val str = editable.toString()
            //Узнаем позицию
            val position = str.indexOf(".")
            //Если точка есть делаем проверки
            if (position != -1) {
                //Отрезаем кусок строки начиная с точки и до конца строки
                val subStr = str.substring(position)
                //Отрезаем строку с начала и до точки
                val subStrStart = str.substring(0, position)
                //Если символов после точки больше чем 3 или если точка первая в строке - удаляем последний
                if (subStr.length > 3 || subStrStart.isEmpty()) {
                    editable.delete(editable.length - 1, editable.length)
                }
            } else {
                //или число больше 7 знаков
                if (str.length > 7) {
                    editable.delete(editable.length - 1, editable.length)
                }
            }
        }
    })
}

fun BottomNavigationView.linksActivity(selectedItemId: Int, activity: Activity) {
    this.selectedItemId = selectedItemId
    this.setOnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            selectedItemId -> {
                true
            }
            R.id.mainActivityMA -> {
                activity.startActivity(Intent(activity, MainActivity::class.java))
                activity.overridePendingTransition(0, 0)
                activity.finish()
                true
            }
            R.id.budgetActivityMA -> {
                activity.startActivity(Intent(activity, BudgetActivity::class.java))
                activity.overridePendingTransition(0, 0)
                activity.finish()
                true
            }
            R.id.expensesActivityMA -> {
                activity.startActivity(Intent(activity, ExpensesActivity::class.java))
                activity.overridePendingTransition(0, 0)
                activity.finish()
                true
            }
            R.id.reportingActivityMA -> {
                activity.startActivity(Intent(activity, ReportingActivity::class.java))
                activity.overridePendingTransition(0, 0)
                activity.finish()
                true
            }
            R.id.moreActivityMA -> {
                activity.startActivity(Intent(activity, MoreActivity::class.java))
                activity.overridePendingTransition(0, 0)
                activity.finish()
                true
            }
            else -> {
                true
            }
        }
    }
}