package com.reznik.hotrit.utils

/**
 * Класс для группировки расходов
 */
class ExpensesGroupKey(
    var articleId: Long? = null
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ExpensesGroupKey

        if (articleId != other.articleId) return false

        return true
    }

    override fun hashCode(): Int {
        return articleId?.hashCode() ?: 0
    }
}