package com.reznik.hotrit.utils

import androidx.room.TypeConverter
import java.util.*

/**
 * Конвертеры для ORM ROOM (правила для конвертации внешних объектов)
 */
class Converters {
    @TypeConverter
    fun fromTimestamp(value: Long?): Date? {
        return value?.let { Date(it) }
    }

    @TypeConverter
    fun dateToTimestamp(date: Date?): Long? {
        return date?.time
    }

    @TypeConverter
    fun uuidToString(value: UUID?): String? {
        return value?.toString()
    }

    @TypeConverter
    fun stringToUuid(value: String?): UUID? {
        return UUID.fromString(value)
    }
}